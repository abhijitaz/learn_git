from pyhere import here
from pathlib import Path
import pandas as pd

datadir = here('data/raw')

data_clinical = pd.read_csv(Path(datadir,'clinical_data.csv'))
data_bioinfo = pd.read_csv(Path(datadir, 'expressions.csv'))

data_merged = data_clinical.merge(data_bioinfo, 
        right_on = 'TCGA_ID', left_on = 'Complete TCGA ID')

data_merged.to_csv(here('data','processed','analytic.csv'))

