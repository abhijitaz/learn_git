from pyhere import here
from pathlib import Path

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

datadir = here('data','processed')
figdir = here('reports','figures')

dat = pd.read_csv(Path(datadir, 'analytic.csv'))

# Look at distribution of protein expressions

protein_expressions = dat.filter(regex = ('NP.*'))
expressions_melted = protein_expressions.melt(var_name = 'protein', value_name='expression')

fig, ax = plt.subplots(figsize=(12,8))
sns.boxplot(ax=ax, data = expressions_melted, y = 'protein', x = 'expression',)
plt.show()

