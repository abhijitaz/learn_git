from pyhere import here
from pathlib import Path
import klib
import numpy as np
import pandas as pd

datadir = here('data','processed')

dat = pd.read_csv(Path(datadir,'analytic.csv'))
dat = klib.clean_column_names(dat)

dat['brca_types'] = 'Other'
dat['brca_types'][(dat['er_status']=='Positive') | (dat['pr_status']=='Positive')] = 'Luminal'
dat['brca_types'][(dat['her2_final_status']=="Positive")] = 'HER2'
dat['brca_types'][(dat['er_status']=="Negative") & (dat['pr_status']=='Negative') & (dat['her2_final_status']=='Negative')] = 'Basal-like'

dat['tnm'] = dat['tumor']+dat['node']+dat['metastasis']
